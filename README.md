# Localization And Mapping

This package will contain the libraries and components to perform mapping and localization.

## Migration status

### Move code from RobotComponents package

- [ ] Self-localization library
- [ ] Cartographer integration library
- [ ] Cartographer component 
- [ ] Map registration library
- [ ] Map registration component

Decide on the legacy components

- [ ] Hector mapping


## Features to be added

- [ ] Mapping / Localization memory for occupancy grids (currently part of the vision memory)
