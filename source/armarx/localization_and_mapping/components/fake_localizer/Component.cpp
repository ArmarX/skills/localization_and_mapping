/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    localization_and_mapping::ArmarXObjects::fake_localizer
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"
#include "RobotComponents/libraries/SelfLocalization/SelfLocalization.h"

#include <Eigen/Geometry>

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <IceUtil/Time.h>

// Include headers you only need in function definitions in the .cpp.

// #include <Eigen/Core>

// #include <SimoxUtility/color/Color.h>


namespace armarx::localization_and_mapping::components::fake_localizer
{

    const std::string
    Component::defaultName = "FakeLocalizer";


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def = SelfLocalization::createPropertyDefinitions();

        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        // def->component(myComponentProxy)


        def->required(properties.globalPose.x, "p.pose.x", "x coordinate [mm] of the global pose");
        def->required(properties.globalPose.y, "p.pose.y", "y coordinate [mm] of the global pose");
        def->required(properties.globalPose.yaw, "p.pose.yaw", "yaw coordinate [rad] of the global pose");

        def->optional(properties.reportPeriodMs, "p.reportPeriodMs", "Period [ms] to report fake pose");

        return def;
    }


    void
    Component::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);

        SelfLocalization::onInitComponent();
    }

    void
    Component::onConnectComponent()
    {
        SelfLocalization::onConnectComponent();

        // Do things after connecting to topics and components.

        /* (Requies the armarx::DebugObserverComponentPluginUser.)
        // Use the debug observer to log data over time.
        // The data can be viewed in the ObserverView and the LivePlotter.
        // (Before starting any threads, we don't need to lock mutexes.)
        {
            setDebugObserverDatafield("numBoxes", properties.numBoxes);
            setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
            sendDebugObserverBatch();
        }
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
        // Draw boxes in ArViz.
        // (Before starting any threads, we don't need to lock mutexes.)
        drawBoxes(properties, arviz);
        */

        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        // Setup the remote GUI.
        {
            createRemoteGuiTab();
            RemoteGui_startRunningTask();
        }
        */

        if(properties.reportPeriodMs <= 0)
        {
            ARMARX_INFO << "Will report fake pose only once";
        }
        else
        {
            localizationReportTask =  new armarx::SimplePeriodicTask<>(
                [&]()
            {
                reportFakePose();
            }, properties.reportPeriodMs);

            localizationReportTask->start();
        }

    }


    void
    Component::onDisconnectComponent()
    {
        SelfLocalization::onDisconnectComponent();
    }


    void
    Component::onExitComponent()
    {
        SelfLocalization::onExitComponent();
    }


    Eigen::Affine3f 
    Component::worldToMapTransform() const
    {
        return Eigen::Affine3f::Identity();
    }


    void
    Component::reportFakePose()
    {
        armarx::PoseStamped pose;
        pose.timestamp = IceUtil::Time::now();

        pose.pose.setIdentity();
        pose.pose.translation().x() = properties.globalPose.x;
        pose.pose.translation().y() = properties.globalPose.y;
        pose.pose.linear() = Eigen::AngleAxisf(properties.globalPose.yaw, Eigen::Vector3f::UnitZ()).toRotationMatrix();

        publishSelfLocalization(pose);
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
    void
    Component::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        tab.boxLayerName.setValue(properties.boxLayerName);

        tab.numBoxes.setValue(properties.numBoxes);
        tab.numBoxes.setRange(0, 100);

        tab.drawBoxes.setLabel("Draw Boxes");

        // Setup the layout.

        GridLayout grid;
        int row = 0;
        {
            grid.add(Label("Box Layer"), {row, 0}).add(tab.boxLayerName, {row, 1});
            ++row;

            grid.add(Label("Num Boxes"), {row, 0}).add(tab.numBoxes, {row, 1});
            ++row;

            grid.add(tab.drawBoxes, {row, 0}, {2, 1});
            ++row;
        }

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void
    Component::RemoteGui_update()
    {
        if (tab.boxLayerName.hasValueChanged() || tab.numBoxes.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.boxLayerName = tab.boxLayerName.getValue();
            properties.numBoxes = tab.numBoxes.getValue();

            {
                setDebugObserverDatafield("numBoxes", properties.numBoxes);
                setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
                sendDebugObserverBatch();
            }
        }
        if (tab.drawBoxes.wasClicked())
        {
            // Lock shared variables in methods running in seperate threads
            // and pass them to functions. This way, the called functions do
            // not need to think about locking.
            std::scoped_lock lock(propertiesMutex, arvizMutex);
            drawBoxes(properties, arviz);
        }
    }
    */


    /* (Requires the armarx::ArVizComponentPluginUser.)
    void
    Component::drawBoxes(const Component::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */


    ARMARX_DECOUPLED_REGISTER_COMPONENT(Component);

}  // namespace armarx::localization_and_mapping::components::fake_localizer
