/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    localization_and_mapping::ArmarXObjects::fake_localizer
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// #include <mutex>

#include <Eigen/Core>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

// #include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

// #include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

// #include <armarx/localization_and_mapping/components/fake_localizer/ComponentInterface.h>
#include <RobotComponents/libraries/SelfLocalization/SelfLocalization.h>

namespace armarx::localization_and_mapping::components::fake_localizer
{

    class Component :
        virtual public armarx::Component,
        // virtual public armarx::localization_and_mapping::components::fake_localizer::ComponentInterface,
        // , virtual public armarx::DebugObserverComponentPluginUser
        // , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        // , virtual public armarx::ArVizComponentPluginUser
        virtual public armarx::SelfLocalization
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


        /* (Requires armarx::LightweightRemoteGuiComponentPluginUser.)
        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab();

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;
        */

        Eigen::Affine3f worldToMapTransform() const override;


    private:

        // Private methods go here.

        // Forward declare `Properties` if you used it before its defined.
        // struct Properties;

        void reportFakePose();

        /* (Requires the armarx::ArVizComponentPluginUser.)
        /// Draw some boxes in ArViz.
        void drawBoxes(const Properties& p, viz::Client& arviz);
        */


    private:

	static const std::string defaultName;

        // Private member variables go here.


        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            struct GlobalPose
            {
                float x;
                float y;
                float yaw;
            };

            GlobalPose globalPose;

            int64_t reportPeriodMs{100};

        };
        Properties properties;
        /* Use a mutex if you access variables from different threads
         * (e.g. ice functions and RemoteGui_update()).
        std::mutex propertiesMutex;
        */


        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        /// Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::LineEdit boxLayerName;
            armarx::RemoteGui::Client::IntSpinBox numBoxes;

            armarx::RemoteGui::Client::Button drawBoxes;
        };
        RemoteGuiTab tab;
        */


        /* (Requires the armarx::ArVizComponentPluginUser.)
         * When used from different threads, an ArViz client needs to be synchronized.
        /// Protects the arviz client inherited from the ArViz plugin.
        std::mutex arvizMutex;
        */

        armarx::SimplePeriodicTask<>::pointer_type localizationReportTask;

    };

}  // namespace armarx::localization_and_mapping::components::fake_localizer
